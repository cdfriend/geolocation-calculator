public class LocationCalculator{
	
	private static final double H_AOV = 29.8; //degrees
	private static final double V_AOV = 15.91; //degrees
	private static final int H_RES = 4096; //pixels
	private static final int V_RES = 2160; //pixels
	//camera constants
	
	private static final int EARTH_RADIUS = 6371000; //meters

	public static void main(String[] args) {
	}
	
	private static double haversine(double lat1, double long1, double lat2, double long2)
	{ //in degrees
		double l1 = Math.toRadians(lat1);
		double l2 = Math.toRadians(lat2);
		double latChange = Math.toRadians(lat2 - lat1);
		double longChange = Math.toRadians(long2 - long1);
		double a = (1 - Math.cos(latChange)) / 2 + Math.cos(l1) * Math.cos(l2) * Math.pow(Math.sin(longChange / 2), 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); //angle made between two points from Earth center
		double d = c * EARTH_RADIUS;
		return d; //returns value in meters
	}
	
	private static int getArea(double altitude)
	{ //returns area in square meters
		double hLength = 2 * altitude * Math.tan(Math.toRadians(H_AOV / 2));
		double vLength = 2 * altitude * Math.tan(Math.toRadians(V_AOV / 2));
		return (int)(hLength * vLength);
	}
	
	public static GPSLoc pixeltoGPS (TelemetryData t, int x, int y)
	{
		final double pitchRads = Math.toRadians(t.pitch);
		final double rollRads = Math.toRadians(t.roll);
		final double yawRads = Math.toRadians(t.yaw);
		final double halfAOVX = Math.toRadians(H_AOV / 2);
		final double halfAOVY = Math.toRadians(V_AOV / 2);
		double maxOffsetX = (t.altitude * Math.sin(halfAOVX)) / (Math.cos(halfAOVX + rollRads) * Math.sin((Math.PI / 2) + rollRads));
		//determine maximum horizontal distance viewed from camera
		double requestedToMaxX = (double)(2 * x) / H_RES;
		//determine ratio of pixel distance from center to max distance from center
		double distX = requestedToMaxX * maxOffsetX;
		//multiply ratio by max distance to get distance in meters
		double maxOffsetY = (t.altitude * Math.sin(halfAOVY)) / (Math.cos(halfAOVY + pitchRads) * Math.sin((Math.PI / 2) + pitchRads));
		double requestedToMaxY = (double)(2 * x) / V_RES;
		double distY = requestedToMaxY * maxOffsetY;
		//repeat process for Y-axis with pitch
		double rotatedX = (distX * Math.sin(yawRads)) + (distY * Math.cos(yawRads));
		double rotatedY = (distX * Math.cos(yawRads)) + (distY * Math.sin(yawRads));
		//rotate distance vectors to account for heading
		double angX = Math.toDegrees(rotatedX / EARTH_RADIUS);
		double angY = Math.toDegrees(rotatedY / EARTH_RADIUS);
		//divide by Earth radius to get latitude and longitude distances in rads (converted to degrees)
		return new GPSLoc(t.location.latitude + angY, t.location.longitude + angX);
	}
}
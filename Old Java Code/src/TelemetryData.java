public class TelemetryData {
	public GPSLoc location;
	public double altitude; //meters
	public double pitch; //degrees, left being positive
	public double roll; //degrees, nose down being positive
	public double yaw; //degrees, 0 = north
}

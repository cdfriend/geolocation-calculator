public class GPSLoc {
	public double latitude;
	public double longitude;
	
	public GPSLoc(double la, double lo)
	{
		latitude = la;
		longitude = lo;
	}
}

# README #

### What is this repository for? ###

* This repository contains a small C# application to write and test an algorithm for the determination of a pixel location in a returned image, as well as the area between several points.  
*** The contents of this repository are now included in the image processing repository.  See this repository only for debugging purposes.**  

### How do I get set up? ###

* At the moment the code contains no dependancies other than System.Math - it should run out of Visual Studio without any particular issue.  

### Who do I talk to? ###

* Charlie Friend <charles.d.friend@gmail.com> - creator of code
* Paul Hunter - head of Image Processing, should be able to provide further information on how these functions will be implemented.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geolocation_Calculator
{
    /// <summary>
    /// Object containing all telemetry information returned from the aircraft with an image.
    /// </summary>
    public class TelemetryData
    {
        /// <value>GPS location of the aircraft.</value>
        public GPSLoc location;
        /// <value>Altitude of the aircraft in meters.</value>
        public double altitude;
        /// <value>Pitch of the aircraft in degrees, nose down being positive.</value>
        public double pitch;
        /// <value>Roll of the aircraft in degrees, left being positive.</value>
        public double roll;
        /// <value>Heading of the aircraft in degrees, 0 being north, 90 being east, etc.</value>
        public double yaw;

        /// <summary>
        /// Class constructor - defines variables to be accessed later.
        /// </summary>
        public TelemetryData(GPSLoc loc, double alt, double p, double r, double y)
        {
            location = loc;
            altitude = alt;
            pitch = p;
            roll = r;
            yaw = y;
        }
    }
}

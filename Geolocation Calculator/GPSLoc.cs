﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geolocation_Calculator
{
    /// <summary>
    /// GPS location object.
    /// </summary>
    public class GPSLoc
    {
        /// <value>Latitude value in decimal degrees, positive being North.</value>
        public double latitude; //North-South
        /// <value>Longitude value in decimal degrees, positive being East.</value>
        public double longitude; //East-West

        /// <summary>
        /// Class constructor - assigns variables for later access.
        /// </summary>
        public GPSLoc(double la, double lo)
        {
            latitude = la;
            longitude = lo;
        }
        /// <summary>
        /// Returns the string value of a GPS location in the Degrees-Minutes-Seconds format.
        /// </summary>
        public String toDegreesMinutesSeconds()
        {
            float latSeconds = (float)(Math.Round(latitude * 3600, 4));
            //get total number of seconds
            int latDegrees = (int)(latSeconds / 3600);
            latSeconds %= 60;
            //latSeconds becomes total remaining minutes and seconds
            int latMinutes = (int)(latSeconds / 60);
            latSeconds %= 60;
            String latDirection = "E";
            if (latitude < 0)
            {
                latDirection = "W";
            } //determine direction of latitude
            float longSeconds = (float)(Math.Round(longitude * 3600, 4));
            //get total number of seconds
            int longDegrees = (int)(longSeconds / 3600);
            longSeconds %= 60;
            //latSeconds becomes total remaining minutes and seconds
            int longMinutes = (int)(longSeconds / 60);
            longSeconds %= 60;
            String longDirection = "N";
            if (longitude < 0)
            {
                longDirection = "S";
            } //determine direction of longitude
            return latDegrees + "° " + latMinutes + "\" " + latSeconds + "' " + latDirection + ", "
                + longDegrees + "° " + longMinutes + "\" " + longSeconds + "' " + longDirection;
        }
    }
}

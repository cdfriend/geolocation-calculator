﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Geolocation_Calculator
{
    /// <summary>
    /// The LocationCalculator class is used to calculate the GPS coordinates of pixels in an image returned from the aircraft.  
    /// </summary>
    public class LocationCalculator
    {

        /// <value>The camera's horizontal angle of view in degrees.</value>
        private const double H_AOV = 29.8;
        /// <value>The camera's vertical angle of view in degrees.</value>
        private const double V_AOV = 15.91;
        /// <value>The horizontal resolution of returned images in pixels.</value>
        private const int H_RES = 4096;
        /// <value>The vertical resolution of returned images in pixels.</value>
        private const int V_RES = 2160;

        /// <summary>
        /// Used to adjust various location-specific values in almost all functions.
        /// </summary>
        private const int EARTH_RADIUS = 6371000; //meters  
        private const double COMPASS_DECLINATION = 0; //magnetic north's deviation from true north (degrees clockwise)

        static void Main()
        {
            //Test aircraft constants
            GPSLoc AC_LOC = new GPSLoc(60, 60);
            const double ALT = 100;
            const double PITCH = 0;
            const double ROLL = 0;
            const double YAW = 0;
            TelemetryData t = new TelemetryData(AC_LOC, ALT, PITCH, ROLL, YAW);
            //GPSLoc output = pixelToGPS(t, 100, 100);
            //Console.WriteLine("Lat: " + output.latitude + " Long: " + output.longitude);
            //Console.WriteLine("Output: " + output.toDegreesMinutesSeconds());
            Point[] testPoints = new Point[5];
            testPoints[0] = new Point(100, 100);
            testPoints[1] = new Point(100, 2060);
            testPoints[2] = new Point(3996, 2060);
            testPoints[3] = new Point(1000, 1000);
            testPoints[4] = new Point(3996, 100);
            Console.WriteLine("Area: " + getArea(t, testPoints));
            String s = Console.ReadLine();
        }
        /// <summary>
        /// Determines the distance between two GPS locations in meters.  
        /// </summary>
        /// <param name="loc1">Latitude and longitude coordinates of the first location.</param>
        /// <param name="loc2">Latitude and longitude coordinates of the second location.</param>
        /// <returns>Distance value in meters.</returns>
        private static double haversine(GPSLoc loc1, GPSLoc loc2)
        { //in degrees
            double l1 = degreesToRads(loc1.latitude);
            double l2 = degreesToRads(loc2.latitude);
            double latChange = degreesToRads(loc2.latitude - loc1.latitude);
            double longChange = degreesToRads(loc2.longitude - loc1.longitude);
            double tanRatio = (1 - Math.Cos(latChange)) / 2 + Math.Cos(l1) * Math.Cos(l2) * Math.Pow(Math.Sin(longChange / 2), 2);
            double centralAngle = 2 * Math.Atan2(Math.Sqrt(tanRatio), Math.Sqrt(1 - tanRatio)); //angle made between two points from Earth center
            double distance = centralAngle * EARTH_RADIUS;
            return distance; //returns value in meters
        }

        
        /// <summary>
        /// Calculates the area of a polygon from a returned image.  
        /// <para>Uses <see cref="pixelToGPS"/> in order to account for the angle at which the image was taken.</para>
        /// <para>Note: this algorithm will produce an incorrect answer for self-intersecting polygons.</para>
        /// <para>All points used in the input MUST BE UNIQUE.  Two of the same point will cause the output to be zero.</para>
        /// </summary>
        /// <param name="t">Telemetry data from Airframe</param>
        /// <param name="corners">The pixel location of all corners on the requested polygon.</param>
        /// <returns>Area of the requested polygon in square meters.</returns>
        private static double getArea(TelemetryData t, Point[] corners)
        { //returns area in square meters
            double area = 0;
            GPSLoc[] GPSCorners = new GPSLoc[corners.Length];
            Console.WriteLine(corners.Length + " corners in polygon");
            for (int i = 0; i < corners.Length; i++)
            {
                GPSCorners[i] = pixelToGPS(t, corners[i].X, corners[i].Y);
                Console.WriteLine("Corner " + i + ": " + GPSCorners[i].toDegreesMinutesSeconds());
                //convert all image points into GPS locations
            }
            double farthestPointSouth = GPSCorners[0].latitude;
            double farthestPointWest = GPSCorners[0].longitude;
            //set farthest points East and South to be the first array value
            for (int i = 1; i < GPSCorners.Length; i++)
            {
                if (GPSCorners[i].latitude < farthestPointSouth)
                {
                    farthestPointSouth = GPSCorners[i].latitude;
                }

                if (GPSCorners[i].longitude < farthestPointWest)
                {
                    farthestPointWest = GPSCorners[i].longitude;
                }

            } //determine the farthest east and south long and lat values, to be used as the x and y axis
            Console.WriteLine("Lowest Latitude: " + farthestPointSouth);
            Console.WriteLine("Lowest Longitude: " + farthestPointWest);
            double[] metersX = new double[corners.Length];
            double[] metersY = new double[corners.Length];
            //initialize two arrays of X and Y variables for a Cartesian coordinate system in meters
            for (int i = 0; i < corners.Length; i++)
            {
                metersX[i] = degreesToRads(GPSCorners[i].longitude - farthestPointWest) * EARTH_RADIUS;
                metersY[i] = degreesToRads(GPSCorners[i].latitude - farthestPointSouth) * EARTH_RADIUS;
                Console.WriteLine("Corner " + i + ": (" + metersX[i] + ", " + metersY[i] + ")");
            } //calculate distance in meters from X and Y axis for all points
            for (int i = 0; i < corners.Length - 1; i++)
            {
                area += metersX[i] * metersY[i + 1] - metersY[i] * metersX[i + 1];
            } //determine area using trapezoid method, due to geometry formula for loop returns double the area
            return Math.Abs(area);
        }

        /// <summary>
        /// Used to determine the GPS coordinates of any given point in a returned image from the aircraft.
        /// </summary>
        /// <param name="t">All telemetry data returned from the aircraft when the photo was taken.  This includes aircraft location, altitude, and orientation.</param>
        /// <param name="x">The x-coordinate of the requested pixel.</param>
        /// <param name="y">The y-coordinate of the requested pixel.</param>
        /// <returns>GPS location of requested pixel.</returns>
        public static GPSLoc pixelToGPS(TelemetryData t, int x, int y)
        {
            x -= (H_RES / 2);
            Console.WriteLine("X Relative To Center: " + x);
            y -= (V_RES / 2);
            y *= -1; //negate y value so values above image center are positive
            Console.WriteLine("Y Relative To Center: " + y);
            //compare the requested pixel to top-left rather than center of image
            double pitchRads = degreesToRads(t.pitch);
            double rollRads = degreesToRads(t.roll);
            double yawRads = degreesToRads(t.yaw + COMPASS_DECLINATION + 90);
            double halfAOVX = degreesToRads(H_AOV / 2);
            double halfAOVY = degreesToRads(V_AOV / 2);
            double maxOffsetX = (t.altitude * Math.Sin(halfAOVX)) / (Math.Cos(halfAOVX + rollRads) * Math.Sin((Math.PI / 2) + rollRads));
            //determine maximum horizontal distance viewed from camera
            double requestedToMaxX = (double)(2 * x) / H_RES;
            //determine ratio of pixel distance from center to max distance from center
            double distX = requestedToMaxX * maxOffsetX;
            Console.WriteLine(distX + " meters horizontally from aircraft location");
            //multiply ratio by max distance to get distance in meters
            double maxOffsetY = (t.altitude * Math.Sin(halfAOVY)) / (Math.Cos(halfAOVY + pitchRads) * Math.Sin((Math.PI / 2) + pitchRads));
            double requestedToMaxY = (double)(2 * y) / V_RES;
            double distY = requestedToMaxY * maxOffsetY;
            Console.WriteLine(distY + " meters vertically from aircraft location");
            //repeat process for Y-axis with pitch
            double rotatedX = (distX * Math.Sin(yawRads)) + (distY * Math.Cos(yawRads));
            Console.WriteLine(rotatedX + " meters East of aircraft location");
            double rotatedY = (distX * Math.Cos(yawRads)) + (distY * Math.Sin(yawRads));
            Console.WriteLine(rotatedY + " meters North of aircraft location");
            //rotate distance vectors to account for heading
            double angX = radsToDegrees(rotatedX / EARTH_RADIUS);
            double angY = radsToDegrees(rotatedY / EARTH_RADIUS);
            //divide by Earth radius to get latitude and longitude distances in rads (converted to degrees)
            return new GPSLoc(t.location.latitude + angY, t.location.longitude + angX);
        }

        /// <summary>
        /// Converts an angle in degrees to radians.
        /// </summary>
        /// <param name="degrees">Angle in decimal degrees</param>
        /// <returns>Value in radians.</returns>
        private static double degreesToRads(double degrees)
        {
            return (degrees * (Math.PI / 180));
        }

        /// <summary>
        /// Converts an angle in radians to degrees.
        /// </summary>
        /// <param name="rads">Angle in radians</param>
        /// <returns>Angle in degrees</returns>
        private static double radsToDegrees(double rads)
        {
            return (rads * (180 / Math.PI));
        }
    }
}